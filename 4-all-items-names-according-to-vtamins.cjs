const data=require('./3-arrays-vitamins.cjs')

const allItemsAccToVitamins=data.reduce((acc,data)=>{
    if((data.contains).includes("Vitamin A")){
        acc["Vitamin A"].push(data.name)
    }
    if((data.contains).includes("Vitamin B")){
        acc["Vitamin B"].push(data.name)
    }
    if((data.contains).includes("Vitamin C")){
        acc["Vitamin C"].push(data.name)
    }
    if((data.contains).includes("Vitamin D")){
        acc["Vitamin D"].push(data.name)
    }
    if((data.contains).includes("Vitamin K")){
        acc["Vitamin K"].push(data.name)
    }
    return acc
},{
    "Vitamin A": [],
    "Vitamin B": [],
    "Vitamin C": [],
    "Vitamin D": [],
    "Vitamin K": [],
})

console.log(allItemsAccToVitamins);